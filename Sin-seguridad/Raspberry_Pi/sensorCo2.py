import board
import busio
import adafruit_sgp30
import os.path
import sys
import time
import paho.mqtt.client

def lectura_sensor(sgp30_sensor, client = None):
    
    # Obtencion de los valores actuales del sensor
    eCO2, TVOC = sgp30_sensor.iaq_measure()
    print("eCO2 = %d ppm \t TVOC = %d ppb" % (eCO2, TVOC))
    if client != None:
        # Envio de datos al broker
        client.publish("interior/eco2-sensor", eCO2)
        client.publish("interior/tvoc-sensor", TVOC)
        
    time.sleep(1)

# Definimos las opciones del sensor y creamos un objeto que lo represente
i2c = busio.I2C(board.SCL, board.SDA, frequency=100000)
sgp30_sensor = adafruit_sgp30.Adafruit_SGP30(i2c)
sgp30_sensor.iaq_init()

# Definimos la IP del broker y creamos un cliente
broker_ip = "192.168.1.71"
client = paho.mqtt.client.Client("Cliente_sensor")

if len(sys.argv) == 2:

    if sys.argv[1] == "-c":
        inicio = time.time()
        transcurrido = 0
        while transcurrido < 60: #Son necesarias 12h que son 43200s
            lectura_sensor(sgp30_sensor)
            transcurrido = time.time() - inicio
        
        print(
            "##### Valores baseline: eCO2 = 0x%x, TVOC = 0x%x #####"
            % (sgp30_sensor.baseline_eCO2, sgp30_sensor.baseline_TVOC)
        )
        #Abrir archivo para escritura de baseline
        baseline = open("baseline.dat", "w")
        #Guardar datos en Hexa de eCO2 y TVOC
        baseline.write(str(hex(sgp30_sensor.baseline_eCO2))+'\n'+str(hex(sgp30_sensor.baseline_TVOC)))
        #Cerrar archivo    
        baseline.close()
        sys.exit("Valores baseline guardados en el archivo baseline.dat, finalizando...")
        
    # Cargamos datos baseline
    elif sys.argv[1] == "-b":
        baseline = open("baseline.dat", "r")
        eCO2 = int(baseline.readline().rstrip('\n'), 16)
        TVOC = int(baseline.readline(), 16)
        #print("%x, %x" % (eCO2, TVOC))
        sgp30_sensor.set_iaq_baseline(eCO2, TVOC)
        baseline.close()
        # Conexion con el broker de MQTT para envio de datos del sensor
        client.connect(broker_ip)
        #Leemos el sensor
        while True:
            lectura_sensor(sgp30_sensor, client)
    
    # Opcion para testeo
    elif sys.argv[1] == "-t":
        sgp30_sensor.set_iaq_baseline(0x8973, 0x8AAE)
        while True:
            lectura_sensor(sgp30_sensor)
    
    else:
        print("Opcion no valida, finalizando...")
        
else:
    print("Para ejecutar el programa utilice primero la opcion -c para calibrar, luego la opcion -b para empezar a medir")
    
    
